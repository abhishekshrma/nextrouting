/* eslint-disable react-hooks/exhaustive-deps */
"use client";

import React, {
  ReactNode,
  createContext,
  useContext,
  useMemo,
  useState,
} from "react";

export type CartItem = {
  id: number;
  name: string;
  price: number;
  quantity: number;
  image: string;
};

interface CartContextType {
  cart: CartItem[];
  addToCart: (item: CartItem, quantity?: number) => void;
  removeFromCart: (id: number) => void;
}

const initialCartState: CartItem[] = [];

const CartContext = createContext<CartContextType>({
  cart: initialCartState,
  addToCart: () => {},
  removeFromCart: () => {},
});

export const CartContextProvider = ({ children }: { children: ReactNode }) => {
  const [cart, setCart] = useState<CartItem[]>([]);

  const addToCart = (item: CartItem, quantity: number = 1) => {
    const existingItemIndex = cart.findIndex(
      (cartItem) => cartItem.id === item.id
    );

    if (existingItemIndex !== -1) {
      const updatedCart = [...cart];
      updatedCart[existingItemIndex].quantity = quantity;
      setCart(updatedCart);
    } else {
      setCart((prevItems) => [...prevItems, { ...item, quantity }]);
    }
  };

  const removeFromCart = (id: number) => {
    setCart(cart.filter((cartItem) => cartItem.id !== id));
  };

  const contextValue: CartContextType = useMemo(() => {
    return {
      cart,
      addToCart,
      removeFromCart,
    };
  }, [cart]);

  return (
    <CartContext.Provider value={contextValue}>{children}</CartContext.Provider>
  );
};

export default CartContextProvider;

export const useCartContext = () => useContext(CartContext);
