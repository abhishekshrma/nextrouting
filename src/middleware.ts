import { cookies } from "next/headers";
import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

export function middleware(request: NextRequest) {
  if (request.nextUrl.pathname.startsWith("/profile")) {
    const isLoggedIn = cookies().get("isLoggedIn");
    if (!isLoggedIn?.value) {
      return NextResponse.redirect(new URL("/login", request.url));
    }
  }

  if (request.nextUrl.pathname.startsWith("/login")) {
    const isLoggedIn = cookies().get("isLoggedIn");
    if (isLoggedIn?.value) {
      return NextResponse.redirect(new URL("/profile", request.url));
    }
  }
}
