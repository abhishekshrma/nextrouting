"use client";

import React, { useLayoutEffect, useState } from "react";
import { IPOST } from "@/types/posts";
import { useCartContext } from "@/context/carts.context";

// Define the props interface
type AddToCartProps = {
  product: Pick<IPOST, "id" | "title" | "price" | "image">;
  currentQuantity?: number;
};

// AddToCart component
const AddToCart = ({ product, currentQuantity = 1 }: AddToCartProps) => {
  const [quantity, setQuantity] = useState(currentQuantity);
  const { cart, addToCart } = useCartContext();

  // Handle click event for "Add to Cart" button
  const handleAddToCart = () => {
    const itemToAdd = {
      id: product.id,
      name: product.title,
      price: product.price,
      quantity: quantity,
      image: product.image,
    };

    addToCart(itemToAdd , quantity);
    // setQuantity(1);
  };

  useLayoutEffect(() => {
    const currentCart = cart.find((item) => item.id === product.id);
    if (currentCart) {
      setQuantity(currentCart.quantity);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[cart]);

  return (
    <div className="flex justify-center items-center w-full gap-10">
      {/* Quantity Input */}
      <div className="flex items-center">
        <label className="mr-2 text-xl font-semibold leading-7">Quantity:</label>
        <div className="flex border rounded overflow-hidden">
          <button
            className="bg-gray-200 text-gray-700 px-4 py-2"
            onClick={() => setQuantity(Math.max(1, quantity - 1))}
          >
            -
          </button>
          <input
            type="number"
            className="appearance-none border-none w-16 py-2 text-center text-gray-700"
            value={quantity}
            onChange={(e) =>
              setQuantity(Math.max(1, parseInt(e.target.value) || 0))
            }
          />
          {/* Increase Button */}
          <button
            className="bg-gray-200 text-gray-700 px-4 py-2"
            onClick={() => setQuantity(quantity + 1)}
          >
            +
          </button>
        </div>
      </div>

      {/* "Add to Cart" Button */}
      <button
        className="bg-yellow-500 text-white py-2 px-4 rounded-md hover:bg-transparent hover:border hover:border-yellow-500 hover:text-yellow-500"
        onClick={handleAddToCart}
      >
        Add to Cart
      </button>
    </div>
  );
};

export default AddToCart;
