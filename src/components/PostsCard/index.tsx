import { IPOST } from "@/types/posts";
import Image from "next/image";
import Link from "next/link";
import { twMerge } from "tailwind-merge";
import AddToCart from "../AddToCart";

type postsCardProps = {
  className?: string;
  contentClassName?: string;
  post: IPOST;
};

const PostCard = ({ className, contentClassName, post }: postsCardProps) => {
  return (
    <div className={ twMerge("flex flex-col justify-center items-center gap-4", className)}>
      <Link
        href={`/products/${post.id}`}
        className={twMerge("flex justify-center items-center gap-5")}
        key={post.id}
      >
        <div className="flex justify-center items-center flex-col h-80 w-2/4 gap-4 ">
          <Image
            src={post.image}
            alt="posts"
            width={500}
            height={500}
            unoptimized
            className="h-full w-full object-cover"
          />
          <p className="text-xl font-semibold tracking-wider">
            {post.category}
          </p>
        </div>
        <div className="flex-1  flex flex-col gap-2 justify-center items-center">
          <p className="text-xl font-semibold tracking-wider mb-5">
            {post.title}
          </p>
          <p
            className={twMerge(
              `hide-scrollbar overflow-scroll py-1`,
              contentClassName
            )}
          >
            {post.description}
          </p>
          <p className="text-xl font-semibold tracking-wider mb-5">
            price: {post.price}
          </p>
        </div>
      </Link>
      <AddToCart product={post} />
    </div>
  );
};

export default PostCard;
