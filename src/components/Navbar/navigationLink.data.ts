import { HiShoppingCart } from "react-icons/hi";

export const navigationLinks = [
    {
      label: "Home",
      href: "/",
      isPublic: true,
    },
    {
      label: "Products",
      href: "/products",
      isPublic: true,
    },
    {
      label: "Profile",
      href: "/profile",
      loggedIn: true,
    },
    {
      label: "Logout",
      action: "handleLogout",
      loggedIn: true,
    },
    {
      label: "Login",
      href: "/login",
      loggedIn: false,
    },
    {
      label: "Signup",
      href: "/signup",
      loggedIn: false,
    },
    {
      label: "Cart",
      href: "/carts",
      icon: HiShoppingCart,
      isPublic: true,
    },
  ];