import { cookies } from "next/headers";
import Link from "next/link";
import { redirect } from "next/navigation";
import { navigationLinks } from "./navigationLink.data";

const Header = () => {
  const isLoggedIn = cookies().get("isLoggedIn")?.value;

  const handleLogout = async () => {
    "use server";
    console.log("logout clicked");
    cookies().delete("isLoggedIn");
    redirect("/login");
  };

  return (
    <header className="bg-white h-16 flex justify-center items-center shadow-navbar sticky top-0 left-0 right-0 z-[9999]">
      <nav className="h-full flex justify-between container items-center">
        <div>
          <Link href="/" className="text-ct-dark-600 text-2xl font-semibold">
            Next-Routing
          </Link>
        </div>
        <ul className="flex items-center gap-4">
          {navigationLinks
            .filter(
              (link) =>
                link.isPublic ?? (link.loggedIn ? isLoggedIn : !isLoggedIn)
            )
            .map((link, index) => (
              <li key={link.label}>
                {link.action ? (
                  <form
                    action={
                      link.action === "handleLogout" ? handleLogout : undefined
                    }
                  >
                    <button type="submit">{link.label}</button>
                  </form>
                ) : (
                  <Link href={link.href ?? ""} className="text-ct-dark-600">
                    {link.icon ? (
                      <link.icon className="h-8 w-8 fill-yellow-300" />
                    ) : (
                      link.label
                    )}
                  </Link>
                )}
              </li>
            ))}
        </ul>
      </nav>
    </header>
  );
};

export default Header;
