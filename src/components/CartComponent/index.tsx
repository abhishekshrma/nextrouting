import Image from "next/image";
import Link from "next/link";
import { twMerge } from "tailwind-merge";
import AddToCart from "../AddToCart";
import { CartItem } from "@/context/carts.context";

type cartProps = {
  className?: string;
  cartItem: CartItem;
};

const CartComponent = ({ className, cartItem }: cartProps) => {
  return (
    <div
      className={twMerge(
        "flex flex-col justify-center items-center gap-4",
        className
      )}
    >
      <Link
        href={`/products/${cartItem.id}`}
        className={twMerge("flex justify-center items-center gap-5")}
        key={cartItem.id}
      >
        <div className="flex justify-center items-center flex-col h-40 w-2/4 gap-4 ">
          <Image
            src={cartItem.image}
            alt="cartItems"
            width={500}
            height={500}
            unoptimized
            className="h-full w-full object-cover"
          />
        </div>
        <div className="flex-1  flex flex-col gap-2 justify-center items-center">
          <p className="text-xl font-semibold tracking-wider mb-5">
            {cartItem.name}
          </p>
          <p className="text-xl font-semibold tracking-wider">
            price: {cartItem.price}
          </p>
          <p className="text-xl font-semibold tracking-wider mb-5">
            Total Price: ${cartItem.quantity * cartItem.price}
          </p>
        </div>
      </Link>
      <AddToCart
        product={{ ...cartItem, title: cartItem.name }}
        currentQuantity={cartItem.quantity}
      />
    </div>
  );
};

export default CartComponent;
