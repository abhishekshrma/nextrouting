import { redirect } from "next/navigation";

const apiRequest = async <T>(
  url: string,
  config?: RequestInit & { isAuthRequired?: boolean; token?: string },
  credentials: RequestCredentials = "same-origin"
): Promise<T | any> => {
  const defaultHeaders = new Headers();
  defaultHeaders.append("Accept", "application/json");

  if (config?.isAuthRequired) {
    const token = config?.token;
    defaultHeaders.append("Authorization", `Bearer ${token}`);
  }

  const result = await fetch(url, {
    credentials,
    ...config,
    headers: defaultHeaders,
  });

  try {
    const contentType = result.headers.get("content-type");

    const response =
      contentType && !contentType.includes("application/json")
        ? await result.text()
        : await result.json();

    if (!result.ok) {
      if (result.status === 401) {
        redirect("/login");
      }
      const res = await response;

      throw new Error(JSON.stringify(res));
    }
    return await response;
  } catch (error: any) {
    console.log(error?.message, "Error ApiRequest");
  }
};

export default apiRequest;
