import Link from "next/link";
import React from "react";

const NotFoundPage = () => {
  return (
    <div className="flex justify-center flex-col items-center gap-4 h-screen pb-40">
      <p className="text-xl font-semibold tracking-wider">Not Found Go Back To Home Page</p>
      <Link  href={'/'} className="px-5 py-2 bg-blue-300">Home </Link>
    </div>
  );
};

export default NotFoundPage;
