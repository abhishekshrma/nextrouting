import React, { FC, Fragment } from "react";
import { Metadata } from "next";

import PostCard from "@/components/PostsCard";
import apiRequest from "@/helpers/api/request";
import { IPOST } from "@/types/posts";

export const metadata: Metadata = {
  title: "Products",
  description: "Products from Fakestore",
};

const getAllPosts = async (): Promise<IPOST[]> =>
  await apiRequest(`https://fakestoreapi.com/products`);

const PostsPage : FC= async () => {
  const posts = await getAllPosts();
  return (
    <div className="flex justify-center items-center flex-wrap gap-4 mx-auto py-5 pb-20 px-5 md:px-0">
      {posts.map((post: any) => (
        <Fragment key={post.id}>
        <PostCard post={post} className="w-full md:w-2/5  2xl:w-2/6 p-5 shadow-lg h-[600px] justify-center" contentClassName="h-[240px]" />
        </Fragment>
      ))}
    </div>
  );
};

export default PostsPage;
