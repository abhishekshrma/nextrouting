import { FC } from "react";

import PostCard from "@/components/PostsCard";
import apiRequest from "@/helpers/api/request";
import { IPOST } from "@/types/posts";

type Props = {
  params: {
    productId: number;
  };
};

const getPostById = async (id: number): Promise<IPOST> =>
  await apiRequest(`https://fakestoreapi.com/products/${id}`);

export async function generateMetadata({ params }: Props) {
  const product = await getPostById(params.productId);
  return (
    product && {
      title: product.title,
      description: product.description,
    }
  );
}

const page: FC<Props> = async ({ params }) => {
  const product = await getPostById(params.productId);
  return (
    <div className="flex justify-center items-center p-5 min-h-[80vh] ">
      {product ? (
        <PostCard post={product} />
      ) : (
        <p className="text-xl font-semibold tracking-wider">
          No Products Found
        </p>
      )}
    </div>
  );
};

export default page;
