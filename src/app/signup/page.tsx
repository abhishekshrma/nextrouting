import SignUp from '@/components/Signup'
import { Metadata } from 'next';

export const metadata: Metadata = {
    title: "Signup",
  };

const SignUpPage = () => <SignUp />

export default SignUpPage