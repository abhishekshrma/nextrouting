import { cookies } from "next/headers";
import React from "react";

type Props = {};

export async function generateMetadata() { 
  return ({
    title: cookies().get('username')?.value ?? ''
  })
}

const page = (props: Props) => {
  const username = cookies().get("username");
  return (
    <div className="flex justify-center items-center min-h-[400px] flex-col gap-4">
      <p className="text-xl font-semibold tracking-wider">WELCOME BACK</p>
      <div>
        You are logged In as{" "}
        <span className="text-xl font-semibold tracking-wider mb-5">
          {username?.value}{" "}
        </span>
      </div>
    </div>
  );
};

export default page;
