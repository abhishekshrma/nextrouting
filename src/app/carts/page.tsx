"use client";

import React from "react";

import CartComponent from "@/components/CartComponent";
import { useCartContext } from "@/context/carts.context";


const CartPage: React.FC = () => {
  const { cart } = useCartContext();
  return (
    <div className="flex gap-4 items-center flex-wrap justify-center px-5">
      {cart.length ? cart.map((currentCart: any) => (
        <div key={currentCart.id} className="flex gap-4  w-full md:w-2/5  2xl:w-2/6 shadow-lg p-5">
          <CartComponent
            cartItem={currentCart}
            className="h-[300px]"
          />
        </div>
      )): <p className="text-xl font-semibold tracking-wider mt-5">No Cart Items</p>}
    </div>
  );
};

export default CartPage;
