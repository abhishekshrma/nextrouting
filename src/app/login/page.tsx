import Login from '@/components/Login'
import { Metadata } from 'next';

export const metadata: Metadata = {
    title: "Login",
  };

const LoginPage = () => <Login />

export default LoginPage