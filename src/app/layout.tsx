import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import Header from "@/components/Navbar";
import CartContextProvider from "@/context/carts.context";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Next-Routing",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <Header />
        <CartContextProvider>{children}</CartContextProvider>
      </body>
    </html>
  );
}
